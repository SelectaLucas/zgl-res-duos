import React from "react"
import { ImageBackground, Image, StatusBar, Dimensions, KeyboardAvoidingView } from "react-native"
import { Block, Button, Text } from "galio-framework"
import argonTheme from "../../../constants/Theme"
import Images from "../../../constants/Images"
import { InputFormik } from "../../../components/Input"
import * as Yup from 'yup';
import { Formik } from 'formik'
import styles from '../styles/LoginStyle'
import mensagens from '../../../constants/Mensagens'
import { post } from '../../../util/ApiAccess'
import api from './../../../constants/api';
const { height, width } = Dimensions.get("screen")

export default props =>
  <Formik
    initialValues={{ identifier: 'teste@teste.com', password: 'Teste01' }}
    onSubmit={values => {
      post(api.LOGIN, api.LOGIN).then((data) => {
        console.log("data", data)
        props.navigation.navigate("App")
      }).catch(e => {
        console.log("err", e)
      });
    }}
    validationSchema={Yup.object().shape({
      identifier: Yup.string().email(mensagens.email).required(mensagens.obrigatorio),
      password: Yup.string().required(mensagens.obrigatorio),
    })}>
    {(propsFormik) => (
      <Block flex middle>
        <StatusBar hidden />
        <ImageBackground source={Images.Onboarding} style={{ width, height, zIndex: 1 }} >
          <Block flex middle>
            <Block style={styles.loginContainer}>
              <Block flex={.25} middle style={styles.logoContainer}>
                <Image source={Images.LogoPrimaria} style={styles.logo} />
              </Block>
              <Block flex>
                <Block flex center>
                  <KeyboardAvoidingView style={styles.inputsContainer} behavior="padding" enabled >
                    <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                      <InputFormik right placeholder="E-mail..." iconContent={<Block />} name={'identifier'} propsFormik={propsFormik} />
                    </Block>
                    <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                      <InputFormik right placeholder="Senha..." iconContent={<Block />} name={'password'} propsFormik={propsFormik} />
                    </Block>
                    <Block middle width={width * 0.8} style={{ marginBottom: 15 }}>
                      <Text center color={argonTheme.COLORS.PRIMARY} size={16} onPress={_ => props.navigation.navigate("ForgotPassword")} textStyle={{ color: argonTheme.COLORS.PRIMARY }}>
                        Esqueci minha senha
                      </Text>
                    </Block>
                    <Block middle>
                      <Button color="primary" style={styles.loginButton} onPress={propsFormik.handleSubmit}>
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                          ENTRAR
                        </Text>
                      </Button>
                    </Block>
                    <Block middle width={width * 0.8} style={{ marginBottom: 15 }}>
                      <Text style={{ marginTop: 10 }} center color={argonTheme.COLORS.PRIMARY} size={16} onPress={_ => props.navigation.navigate("CreateAccount")}>
                        Criar conta
                    </Text>
                    </Block>
                  </KeyboardAvoidingView>
                </Block>
              </Block>
            </Block>
          </Block>
        </ImageBackground>
      </Block>
    )}
  </Formik>