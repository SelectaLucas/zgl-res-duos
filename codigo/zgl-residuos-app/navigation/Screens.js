import React from "react"
import { Easing, Animated, Dimensions } from "react-native"
import { createStackNavigator } from "@react-navigation/stack"
import { createDrawerNavigator } from "@react-navigation/drawer"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { Block } from "galio-framework"
import Home from "../screens/Home"
import Login from "../screens/acesso/pages/Login"
import Pro from "../screens/Pro"
import Profile from "../screens/Profile"
import Register from "../screens/Register"
import Elements from "../screens/Elements"
import Articles from "../screens/Articles"
import ForgotPassword from '../screens/acesso/pages/ForgotPassword'
import CreateAccount from '../screens/acesso/pages/CreateAccount'
import CustomDrawerContent from "./Menu"
import { Icon, Header } from "../components"
import { argonTheme, tabs } from "../constants"

import EmpresaConsulta from "../screens/administracao/empresa/pages/Consulta"
import EmpresaCadastro from "../screens/administracao/empresa/pages/Cadastro"
import ResiduoConsulta from "../screens/administracao/residuo/pages/Consulta"
import ResiduoCadastro from "../screens/administracao/residuo/pages/Cadastro"

const { width } = Dimensions.get("screen")
const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()
const Tab = createBottomTabNavigator()

const ElementsStack = props =>
  <Stack.Navigator mode="card" headerMode="screen">
    <Stack.Screen name="Elements" component={Elements}
      options={{
        header: ({ navigation, scene }) =>
          (<Header title="Elements" navigation={navigation} scene={scene} />), cardStyle: { backgroundColor: "#F8F9FE" }
      }}
    />
    <Stack.Screen name="Pro" component={Pro}
      options={{
        header: ({ navigation, scene }) => (
          <Header title="" back white transparent navigation={navigation} scene={scene} />
        ),
        headerTransparent: true
      }}
    />
  </Stack.Navigator>

const ArticlesStack = props =>
  <Stack.Navigator mode="card" headerMode="screen">
    <Stack.Screen name="Articles" component={Articles}
      options={{
        header: ({ navigation, scene }) => (
          <Header title="Articles" navigation={navigation} scene={scene} />
        ),
        cardStyle: { backgroundColor: "#F8F9FE" }
      }}
    />
    <Stack.Screen name="Pro" component={Pro}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            title=""
            back
            white
            transparent
            navigation={navigation}
            scene={scene}
          />
        ),
        headerTransparent: true
      }}
    />
  </Stack.Navigator>

const ProfileStack = props =>
  <Stack.Navigator initialRouteName="Profile" mode="card" headerMode="screen">
    <Stack.Screen name="Profile" component={Profile}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            transparent
            white
            title="Profile"
            navigation={navigation}
            scene={scene}
          />
        ),
        cardStyle: { backgroundColor: "#FFFFFF" },
        headerTransparent: true
      }}
    />
    <Stack.Screen name="Pro" component={Pro}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            title=""
            back
            white
            transparent
            navigation={navigation}
            scene={scene}
          />
        ),
        headerTransparent: true
      }}
    />
  </Stack.Navigator>

const EmpresaConsultaStack = props =>
  <Stack.Navigator mode="card" headerMode="screen">
    <Stack.Screen name="Empresa" component={EmpresaConsulta}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            title="Consulta Empresa"
            search
            navigation={navigation}
            scene={scene}
          />
        ),
        cardStyle: { backgroundColor: "#F8F9FE" }
      }}
    />
  </Stack.Navigator>

const EmpresaCadastroStack = props =>
  <Stack.Navigator mode="card" headerMode="screen">
    <Stack.Screen name="EmpresaCadastro" component={EmpresaCadastro}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            title="Cadastro Empresa"
            navigation={navigation}
            scene={scene}
          />
        ),
        cardStyle: { backgroundColor: "#F8F9FE" }
      }}
    />
  </Stack.Navigator>

const ResiduoConsultaStack = props =>
  <Stack.Navigator mode="card" headerMode="screen">
    <Stack.Screen name="Residuo" component={ResiduoConsulta}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            title="Consulta Resíduo"
            search
            navigation={navigation}
            scene={scene}
          />
        ),
        cardStyle: { backgroundColor: "#F8F9FE" }
      }}
    />
  </Stack.Navigator>

const ResiduoCadastroStack = props =>
  <Stack.Navigator mode="card" headerMode="screen">
    <Stack.Screen name="ResiduoCadastro" component={ResiduoCadastro}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            title="Cadastro Resíduo"
            navigation={navigation}
            scene={scene}
          />
        ),
        cardStyle: { backgroundColor: "#F8F9FE" }
      }}
    />
  </Stack.Navigator>

const HomeStack = props =>
  <Stack.Navigator mode="card" headerMode="screen">
    <Stack.Screen name="Home" component={Home}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            title="Home"
            search
            options
            navigation={navigation}
            scene={scene}
          />
        ),
        cardStyle: { backgroundColor: "#F8F9FE" }
      }}
    />
    <Stack.Screen name="Pro" component={Pro}
      options={{
        header: ({ navigation, scene }) => (
          <Header
            title=""
            back
            white
            transparent
            navigation={navigation}
            scene={scene}
          />
        ),
        headerTransparent: true
      }}
    />
  </Stack.Navigator>

const AppStack = props =>
  <Drawer.Navigator style={{ flex: 1 }}
    drawerContent={props => <CustomDrawerContent {...props} />}
    drawerStyle={{
      backgroundColor: "white",
      width: width * 0.8
    }}
    drawerContentOptions={{
      activeTintcolor: "white",
      inactiveTintColor: "#000",
      activeBackgroundColor: "transparent",
      itemStyle: {
        width: width * 0.75,
        backgroundColor: "transparent",
        paddingVertical: 16,
        paddingHorizonal: 12,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        overflow: "hidden"
      },
      labelStyle: {
        fontSize: 18,
        marginLeft: 12,
        fontWeight: "normal"
      }
    }}
    initialRouteName="Home"
  >
    <Drawer.Screen name="Home" component={HomeStack} />
    <Drawer.Screen name="Profile" component={ProfileStack} />
    <Drawer.Screen name="Account" component={Register} />
    <Drawer.Screen name="Elements" component={ElementsStack} />
    <Drawer.Screen name="Articles" component={ArticlesStack} />
    <Drawer.Screen name="Empresa" component={EmpresaConsultaStack} />
    <Drawer.Screen name="EmpresaCadastro" component={EmpresaCadastroStack} />
    <Drawer.Screen name="Residuo" component={ResiduoConsultaStack} />
    <Drawer.Screen name="ResiduoCadastro" component={ResiduoCadastroStack} />
  </Drawer.Navigator>

const CreateAccountStack = props =>
  <Stack.Navigator mode="card" headerMode="none">
    <Stack.Screen name="CreateAccount" component={CreateAccount}
      option={{
        headerTransparent: true
      }}
    />
  </Stack.Navigator>

const ForgotPasswordStack = props =>
  <Stack.Navigator mode="card" headerMode="none">
    <Stack.Screen name="ForgotPassword" component={ForgotPassword}
      option={{
        headerTransparent: true
      }}
    />
  </Stack.Navigator>

export default function OnboardingStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="none">
      <Stack.Screen
        name="Login"
        component={Login}
        option={{
          headerTransparent: true
        }}
      />
      <Stack.Screen name="App" component={AppStack} />
      <Stack.Screen name="ForgotPassword" component={ForgotPasswordStack} />
      <Stack.Screen name="CreateAccount" component={CreateAccountStack} />
    </Stack.Navigator>
  )
}