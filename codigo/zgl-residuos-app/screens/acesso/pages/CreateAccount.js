import React from "react"
import { ImageBackground, Image, StatusBar, Dimensions, KeyboardAvoidingView } from "react-native"
import { Block, Button, Text, theme, Checkbox } from "galio-framework"
import argonTheme from "../../../constants/Theme"
import Images from "../../../constants/Images"
import { InputFormik } from "../../../components/Input"
import * as Yup from 'yup';
import { Formik } from 'formik'
import mensagens from '../../../constants/Mensagens'
import styles from '../styles/CreateAccountStyle'

const { height, width } = Dimensions.get("screen")

export default props =>
    <Formik
        initialValues={{ nome: '', email: '', senha: '' }}
        onSubmit={values => props.navigation.navigate("App")}
        validationSchema={Yup.object().shape({
            nome: Yup.string().required(mensagens.obrigatorio),
            email: Yup.string().email(mensagens.email).required(mensagens.obrigatorio),
            senha: Yup.string().required(mensagens.obrigatorio),
        })}>
        {(propsFormik) => (
            <Block flex middle>
                <StatusBar hidden />
                <ImageBackground source={Images.Onboarding} style={{ width, height, zIndex: 1 }} >
                    <Block flex middle>
                        <Block style={styles.registerContainer}>
                            <Block flex={.25} middle style={styles.logoContainer}>
                                <Image source={Images.LogoPrimaria} style={styles.logo} />
                            </Block>
                            <Block flex>
                                <Block flex center>
                                    <KeyboardAvoidingView style={styles.conteinerCreateAccount} behavior="padding" enabled >
                                        <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                                            <InputFormik right placeholder="Nome..." iconContent={<Block />} name={'nome'} propsFormik={propsFormik} />
                                        </Block>
                                        <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                                            <InputFormik right placeholder="E-mail..." iconContent={<Block />} name={'email'} propsFormik={propsFormik} />
                                        </Block>
                                        <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                                            <InputFormik right placeholder="Senha..." iconContent={<Block />} name={'senha'} propsFormik={propsFormik} />
                                        </Block>
                                        <Block row width={width * 0.8} style={{ marginBottom: 15 }}>
                                            <Checkbox checkboxStyle={{ borderWidth: 3 }} color={argonTheme.COLORS.PRIMARY} label="Eu concordo com as Políticas de Privacidade" />
                                        </Block>
                                        <Block middle>
                                            <Button color="primary" style={styles.createButton} onPress={propsFormik.handleSubmit}>
                                                <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                                                    CADASTRAR
                                                </Text>
                                            </Button>
                                        </Block>
                                    </KeyboardAvoidingView>
                                </Block>
                            </Block>
                        </Block>
                    </Block>
                </ImageBackground>
            </Block>
        )}
    </Formik>