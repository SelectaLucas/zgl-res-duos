import React from "react"
import { StyleSheet } from "react-native"
import PropTypes from 'prop-types'
import { Input, Text } from "galio-framework"
import Icon from './Icon'
import { argonTheme } from "../constants"

class ArInput extends React.Component {
  render() {
    const { shadowless, success, error } = this.props

    const inputStyles = [
      styles.input,
      !shadowless && styles.shadow,
      success && styles.success,
      error && styles.error,
      { ...this.props.style }
    ]

    return (
      <Input placeholder="write something here" placeholderTextColor={argonTheme.COLORS.MUTED} style={inputStyles} olor={argonTheme.COLORS.HEADER}
        iconContent={<Icon size={14} color={argonTheme.COLORS.ICON} name="link" family="AntDesign" />}
        {...this.props}
      />
    )
  }
}

class InputFormik extends React.Component {
  render() {
    const { shadowless, success, error, propsFormik } = this.props

    const inputStyles = [
      styles.input,
      !shadowless && styles.shadow,
      success && styles.success,
      error && styles.error,
      { ...this.props.style }
    ]

    return (
      <>
        <Input placeholder="write something here" placeholderTextColor={argonTheme.COLORS.MUTED} style={inputStyles} color={argonTheme.COLORS.HEADER}
          value={propsFormik.values[this.props.name]}
          onChangeText={propsFormik.handleChange(this.props.name)}
          onBlur={() => propsFormik.setFieldTouched(this.props.name)}
          iconContent={<Icon size={14} color={argonTheme.COLORS.ICON} name="link" family="AntDesign" />}
          {...this.props}
        />
        {propsFormik.touched[this.props.name] && propsFormik.errors[this.props.name] &&
          <Text style={{ fontSize: 10, color: 'red' }}>{propsFormik.errors[this.props.name]}</Text>
        }
      </>
    )
  }
}

ArInput.defaultProps = {
  shadowless: false,
  success: false,
  error: false
}

ArInput.propTypes = {
  shadowless: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.bool
}

const styles = StyleSheet.create({
  input: {
    borderRadius: 4,
    borderColor: argonTheme.COLORS.BORDER,
    height: 44,
    backgroundColor: '#FFFFFF'
  },
  success: {
    borderColor: argonTheme.COLORS.INPUT_SUCCESS,
  },
  error: {
    borderColor: argonTheme.COLORS.INPUT_ERROR,
  },
  shadow: {
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 2,
    shadowOpacity: 0.05,
    elevation: 2,
  }
})

export default ArInput
export { InputFormik }
