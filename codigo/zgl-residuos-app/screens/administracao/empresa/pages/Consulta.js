import React, { useState, useEffect } from "react"
import { ScrollView, StyleSheet, Image } from "react-native";
import { Block, Button, Text, theme } from "galio-framework"
import Style from '../styles/EmpresaStyle'
import { CardConsulta } from "../../../../components/Card";

export default props => {
    const [data, setData] = useState([])

    useEffect(_ => {
        setData([
            { count: 1, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 2, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 3, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 4, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 5, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 6, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 7, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 8, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 9, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 10, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 11, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 12, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 13, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 14, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 15, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 16, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 17, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 18, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 19, title: 'Empresa Teste', description: '60.191.120/0001-45' },
            { count: 20, title: 'Empresa Teste', description: '60.191.120/0001-45' },
        ])
    }, [])

    return (
        <Block flex center>
            <ScrollView showsVerticalScrollIndicator={false} >
                {
                    data.map((x, index) =>
                        <Block flex style={Style.group} key={index}>
                            <Block flex>
                                <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                                    <CardConsulta item={x} horizontal />
                                </Block>
                            </Block>
                        </Block>
                    )
                }
            </ScrollView>
        </Block>
    )
}