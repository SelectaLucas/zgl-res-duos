import React, { useState, useEffect } from "react"
import { ScrollView, StyleSheet, Image } from "react-native";
import { Block, Button, Text, theme } from "galio-framework"
import Style from '../styles/ResiduoStyle'
import { CardConsulta } from "../../../../components/Card";

export default props => {
    const [data, setData] = useState([])

    useEffect(_ => {
        setData([
            { count: 1, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 2, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 3, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 4, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 5, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 6, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 7, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 8, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 9, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 10, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 12, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 13, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 14, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 15, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 16, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 17, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 18, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 19, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
            { count: 20, title: 'Pneu Usado', qtd: '20', valor: 'R$ 100,00' },
        ])
    }, [])

    return (
        <Block flex center>
            <ScrollView showsVerticalScrollIndicator={false} >
                {
                    data.map((x, index) =>
                        <Block flex style={Style.group} key={index}>
                            <Block flex>
                                <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                                    <CardConsulta item={x} horizontal />
                                </Block>
                            </Block>
                        </Block>
                    )
                }
            </ScrollView>
        </Block>
    )
}