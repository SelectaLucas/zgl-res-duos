import { StyleSheet, Dimensions } from "react-native"
import { theme } from "galio-framework"
import argonTheme from "../../../constants/Theme"

const { height, width } = Dimensions.get("screen")

export default StyleSheet.create({
    logo: {
        width: 250,
        height: 75,
        zIndex: 2,
        position: 'relative',
        marginTop: '-50%'
    },
    loginContainer: {
        width: width * 0.9,
        height: height * 0.59,
        backgroundColor: "#F4F5F7",
        borderRadius: 4,
        shadowColor: argonTheme.COLORS.BLACK,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 8,
        shadowOpacity: 0.1,
        elevation: 1,
        overflow: "hidden"
    },
    logoContainer: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: "#8898AA",
        justifyContent: 'flex-end'
    },
    loginButton: {
        width: width * 0.5,
        marginTop: 25,
        marginBottom: 25
    },
    inputsContainer: {
        justifyContent: 'flex-end',
        height: '100%',
    }
});