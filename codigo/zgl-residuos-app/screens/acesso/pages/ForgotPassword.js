import React from "react"
import { ImageBackground, Image, StatusBar, Dimensions, KeyboardAvoidingView } from "react-native"
import { Block, Button, Text } from "galio-framework"
import argonTheme from "../../../constants/Theme"
import Images from "../../../constants/Images"
import { InputFormik } from "../../../components/Input"
import * as Yup from 'yup';
import { Formik } from 'formik'
import styles from '../styles/ForgotPasswordStyle'
import mensagens from '../../../constants/Mensagens'

const { height, width } = Dimensions.get("screen")

export default props =>
    <Formik
        initialValues={{ email: '' }}
        onSubmit={values => props.navigation.navigate("App")}
        validationSchema={Yup.object().shape({
            email: Yup.string().email(mensagens.email).required(mensagens.obrigatorio),
        })}>
        {(propsFormik) => (
            <Block flex middle>
                <StatusBar hidden />
                <ImageBackground source={Images.Onboarding} style={{ width, height, zIndex: 1 }} >
                    <Block flex middle>
                        <Block style={styles.Container}>
                            <Block flex={.25} middle style={styles.logoContainer}>
                                <Image source={Images.LogoPrimaria} style={styles.logo} />
                            </Block>
                            <Block flex>
                                <Block flex center>
                                    <KeyboardAvoidingView style={styles.conteinerEmail} behavior="padding" enabled >
                                        <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                                            <InputFormik right placeholder="E-mail..." iconContent={<Block />} name={'email'} propsFormik={propsFormik} />
                                        </Block>
                                        <Block middle width={width * 0.8} style={{ marginBottom: 15 }}>
                                            <Text style={{ marginTop: 10 }} center color={argonTheme.COLORS.PRIMARY} size={16} onPress={_ => props.navigation.navigate("CreateAccount")}>
                                                Insira o seu e-mail, e caso ele esteja cadastrado enviaremos instruções para você recuperar sua senha.
                                            </Text>
                                        </Block>
                                        <Block middle>
                                            <Button color="primary" style={styles.SendButton} onPress={propsFormik.handleSubmit}>
                                                <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                                                    ENVIAR
                                                </Text>
                                            </Button>
                                        </Block>
                                    </KeyboardAvoidingView>
                                </Block>
                            </Block>
                        </Block>
                    </Block>
                </ImageBackground>
            </Block>
        )}
    </Formik>




// export default props => {
//     const { navigation } = props

//     return (
//         <Block flex style={styles.container}>
//             <StatusBar hidden />
//             <Block flex center>
//                 <ImageBackground
//                     source={Images.Onboarding}
//                     style={{ height, width, zIndex: 1 }}
//                 />
//             </Block>
//             <Block center>
//                 <Image source={Images.LogoOnboarding} style={styles.logo} />
//             </Block>
//             <Block flex space="between" style={styles.padded}>
//                 <Block flex space="around" style={{ zIndex: 2 }}>
//                     <Block style={styles.title}>
//                         <Block>
//                             <Input right placeholder="E-mail..." iconContent={<Block />} />
//                             <Text center color="white" size={16}>
//                                 Insira o seu e-mail, e caso ele esteja cadastrado enviaremos instruções para você recuperar sua senha.
//                             </Text>
//                         </Block>
//                     </Block>
//                     <Block center>
//                         <Button style={styles.button} color={argonTheme.COLORS.WHITE}
//                             onPress={_ => navigation.navigate("App")} textStyle={{ color: argonTheme.COLORS.PRIMARY }} >
//                             Enviar
//                         </Button>
//                     </Block>
//                 </Block>
//             </Block>
//         </Block>
//     )
// }