import React, { useState, useEffect } from "react"
import { Block, Button, Text } from "galio-framework"
import { StatusBar, KeyboardAvoidingView } from "react-native"
import argonTheme from "../../../../constants/Theme"
import { InputFormik } from "../../../../components/Input"
import * as Yup from 'yup';
import { Formik } from 'formik'
import mensagens from '../../../../constants/Mensagens'

export default props =>
    <Formik
        initialValues={{ razao_social: '', nome_fantasia: '', cnpj: '' }}
        onSubmit={values => props.navigation.navigate("Empresa")}
        validationSchema={Yup.object().shape({
            razao_social: Yup.string().required(mensagens.obrigatorio),
            nome_fantasia: Yup.string().required(mensagens.obrigatorio),
            cnpj: Yup.string().required(mensagens.obrigatorio),
        })}>
        {(propsFormik) => (
            <Block flex middle>
                <StatusBar hidden />
                <Block flex middle>
                    <Block>
                        <Block flex>
                            <Block flex center>
                                <KeyboardAvoidingView behavior="padding" enabled >
                                    <Block style={{ flex: 2, marginTop: 30 }}>
                                        <Block style={{ marginBottom: 15 }}>
                                            <InputFormik right placeholder="Razão Social..." iconContent={<Block />} name={'razao_social'} propsFormik={propsFormik} />
                                        </Block>
                                        <Block style={{ marginBottom: 15 }}>
                                            <InputFormik right placeholder="Nome Fantasia..." iconContent={<Block />} name={'nome_fantasia'} propsFormik={propsFormik} />
                                        </Block>
                                        <Block style={{ marginBottom: 15 }}>
                                            <InputFormik placeholder="CNPJ..." iconContent={<Block />} name={'cnpj'} propsFormik={propsFormik} />
                                        </Block>
                                    </Block>
                                    <Block middle style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 15 }}>
                                        <Button color="primary" onPress={propsFormik.handleSubmit}>
                                            <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                                                CADASTRAR
                                            </Text>
                                        </Button>
                                    </Block>
                                </KeyboardAvoidingView>
                            </Block>
                        </Block>
                    </Block>
                </Block>
            </Block>
        )}
    </Formik>