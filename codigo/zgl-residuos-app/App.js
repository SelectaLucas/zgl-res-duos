import React, { useState } from "react"
import { Image } from "react-native"
import { AppLoading } from "expo"
import { Asset } from "expo-asset"
import { Block, GalioProvider } from "galio-framework"
import { NavigationContainer } from "@react-navigation/native"

import { enableScreens } from "react-native-screens"
enableScreens()

import Screens from "./navigation/Screens"
import { Images, articles, argonTheme } from "./constants"

const assetImages = [
  Images.Onboarding,
  Images.LogoOnboarding,
  Images.Logo,
  Images.Pro,
  Images.ArgonLogo,
  Images.iOSLogo,
  Images.androidLogo
]

articles.map(article => assetImages.push(article.image))

const cacheImages = images =>
  images.map(image => {
    if (typeof image === "string")
      return Image.prefetch(image)

    return Asset.fromModule(image).downloadAsync()
  })

export default props => {
  const [isLoadingComplete, setIsLoadingComplete] = useState(false)

  const _loadResourcesAsync = async _ => Promise.all([...cacheImages(assetImages)])

  const _handleLoadingError = error => console.warn(error)

  const _handleFinishLoading = _ => setIsLoadingComplete(true)

  if (isLoadingComplete) {
    return (
      <AppLoading
        startAsync={_loadResourcesAsync}
        onError={_handleLoadingError}
        onFinish={_handleFinishLoading}
      />
    )
  } else {
    return (
      <NavigationContainer>
        <GalioProvider theme={argonTheme}>
          <Block flex>
            <Screens />
          </Block>
        </GalioProvider>
      </NavigationContainer>
    )
  }
}